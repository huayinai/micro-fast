package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsUserRoleMapper;
import com.micro.fast.upms.pojo.UpmsUserRole;
import com.micro.fast.upms.service.UpmsUserRoleService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsUserRoleServiceImpl  extends SsmServiceImpl<UpmsUserRole,Integer,UpmsUserRoleMapper>
implements UpmsUserRoleService<UpmsUserRole,Integer>,InitializingBean {

  @Autowired
  private UpmsUserRoleMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
